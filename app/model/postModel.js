// import mongoose from './mongoose
const mongoose = require('mongoose');
// import schema from './schema
const Schema = mongoose.Schema
// khai báo schema
const postSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    title: {
        type: String,
        unique: true,
        required: true,
    },
    body: {
        type: String,
        required: true,
    },
    userId: {
        type: mongoose.Types.ObjectId,
        ref: "user"
    }
});
//export model
module.exports = mongoose.model("post", postSchema);