// import mongoose from './mongoose
const mongoose = require('mongoose');
// import model from './model   
const userSchema = require('../model/userModel');
// Post create new user
const postUser = (request, response) => {
    let body = request.body;
    if (!body.name) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "name is required"
        })
    }
    if (!body.username) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "username is required"
        })
    }
    if (!body.address.street) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "street is required"
        })
    }
    if (!body.address.suite) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "suite is required"
        })
    }
    if (!body.address.city) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "city is required"
        })
    }
    if (!body.address.zipcode) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "zipcode is required"
        })
    }
    if (!body.address.geo.lat) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "lat is required"
        })
    }
    if (!body.address.geo.lng) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "ing is required"
        })
    }
    if (!body.phone) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "phone is required"
        })
    }
    if (!body.website) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "website is required"
        })
    }
    if (!body.company.name) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "name is required"
        })
    }
    if (!body.company.catchPhrase) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "catchPhrase is required"
        })
    }
    if (!body.company.bs) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "bs is required"
        })
    }
    //B3: Sư dụng cơ sở dữ liệu
    let userCreate = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        username: body.username,
        email: body.email,
        address: body.address,
        phone: body.phone,
        website: body.website,
        company: body.company,
    }
    userSchema.create(userCreate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create user successfully",
                data: data
            })
        }
    })
}

// get all 
const getUser = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    userSchema.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all user success",
                data: data
            })
        }
    })
}
// put all 
const putUser = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.params.userId;
    let body = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "User ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let updateUser = {
        name: body.name,
        username: body.username,
        email: body.email,
        address: body.address,
        phone: body.phone,
        website: body.website,
        company: body.company,
    }
    userSchema.findByIdAndUpdate(userId, updateUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update user success",
                data: data
            })
        }
    })
}
// getUserById all 
const getById = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.params.userId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "Dice History ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    userSchema.findById(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get Dice History by id success",
                data: data
            })
        }
    })
}
// deleteUser all 
const deleteUser = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.params.userId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Drink Id is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    userSchema.findOneAndDelete(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Delete Dice History success"
            })
        }
    })
}





//export 
module.exports = { postUser, getUser, putUser, getById, deleteUser }