// import express from 'express';
const express = require('express');
// tạo app
const app = express();
// khai báo model
const userModel = require('./app/model/userModel');
const postModel = require('./app/model/postModel');
const commentModel = require('./app/model/commentModel');
// import router
const userRouter = require('./app/router/userRouter');
// tạo port
const port = 8000;
// import mongoose from 'mongodb
const mongoose = require('mongoose');
// sử dụng json
app.use(express.json());
// khai báo sử dụng unicode
app.use(express.urlencoded({
    extended: true,
}))
//SỬ dụng mongoose
mongoose.connect("mongodb://localhost:27017/Zigvy", (error) => {
    if (error) {
        throw error
    }
    console.log("mongodb connection established successfully")
})
// sử dụng router
app.use("/", userRouter);
// console app
app.listen(port, () => {
    console.log('listening on port: ' + port);
});